#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
公众号：Test201728

日期: 2020/6/4 9:57

"""

import smtplib
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import configparser
import os
import sys
base_path = os.path.abspath(os.path.join(os.getcwd(), ".."))  # 获取上级目录
sys.path.append(base_path)

#
# def email_to():
#     conf = configparser.ConfigParser()
#     conf.read(base_path + r'\Config\server.ini',
#               encoding='utf-8')  # 读config.ini文件
#
#     EMAIL = [
#         conf.get('email', 'host'),  # 服务
#         conf.get('email', 'username'),  # 用户名
#         conf.get('email', 'password'),  # 密码
#         conf.get('email', 'to_email')  # 收件人
#     ]
#     return EMAIL


def send_email(filename):
    """
    发送邮件的功能函数
    :param filename: 文件的路径
    :param title:   邮件的主题
    :return:
    """
    # 第一步：连接邮箱的smtp服务器，并登录
    smtp = smtplib.SMTP_SSL(host='smtp.163.com', port=465)
    smtp.login(user='xxxx@163.com', password='xxxx')
    # 第二步：构建一封邮件
    # 创建一封多组件的邮件
    msg = MIMEMultipart()
    with open(filename, "rb") as f:
        content = f.read()
    # 创建邮件文本内容
    text_msg = MIMEText("""
    (本邮件是程序自动下发的，请勿回复！)
    
    1、测试项目：xxx
    2、测试人员：xxx
    3、本次测试已完成，请下载附件查看测试报告！""", _charset="utf8")

    # 添加到多组件的邮件中
    msg.attach(text_msg)
    # 创建邮件的附件
    report_file = MIMEApplication(content)
    report_file.add_header('content-disposition',
                           'attachment',
                           filename=os.path.split(filename)[-1])
    # 将附件添加到多组件的邮件中
    msg.attach(report_file)
    # 主题
    msg["Subject"] = '接口自动化测试邮件'
    # 发件人
    msg["From"] = 'xxxx@163.com'
    # 收件人
    msg["To"] = 'xxxx@qq.com'
    # 第三步：发送邮箱
    smtp.send_message(msg, from_addr='xxxx@163.com', to_addrs='xxxx@qq.com')
    print("邮件已发送成功！")
