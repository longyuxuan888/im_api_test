#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from jsonpath_rw import parse
from Util.handle_excel import excel_data
import sys
import os
base_path = os.path.abspath(os.path.join(os.getcwd(), ".."))  # 获取上级目录
sys.path.append(base_path)


def split_data(data):
    '''
    拆分单元格数据
    '''
    case_id = data.split(">")[0]
    rule_data = data.split(">")[1]
    return case_id, rule_data


def depend_data(data):
    '''
    获取依赖结果集
    '''
    case_id = split_data(data)[0]
    row_number = excel_data.get_rows_number(case_id)
    data = excel_data.get_cell_value(row_number, 15)
    return data


def get_depend_data(res_data, key):
    '''
    获取依赖字段
    '''
    res_data = json.loads(res_data)
    json_exe = parse(key)
    madle = json_exe.find(res_data)
    return [math.value for math in madle][0]


def get_data(data):
    '''
    获取依赖数据
    '''
    res_data = depend_data(data)
    rule_data = split_data(data)[1]
    return get_depend_data(res_data, rule_data)


if __name__ == "__main__":
    # print(depend_data("imooc_007>data:banner:id"))
    data = {
        "code": 200,
        "msg": "操作成功",
        "data": "7afcc7f7-6bd4-4de3-878d-cc20402faec1"
    }

    key = 'data'
    print(get_depend_data(data, key))
