#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
公众号：Test201728

日期: 2020/5/12 14:03

"""

from Util.handle_excel import excel_data
import unittest
import ddt
import sys
import os

base_path = os.path.abspath(os.path.join(os.getcwd(), ".."))  # 获取上级目录
sys.path.append(base_path)
data = excel_data.get_excel_data()
#print(data)
#data = [[1,2,3,4,5],[2,3,4,5,6],[3,4,5,6,7],[4,5,6,7,8]]

@ddt.ddt
class TestCase01(unittest.TestCase):
    def setUp(self):
        print("用例执行开始...")

    def tearDown(self):
        print("用例执行结束!")

    @ddt.data(*data)
    def test_01(self,data1):

        #casename,casenum,isrun,method,cookie = data1
        #case编号	作用	是否执行	前置条件  依赖key	url	 method	 请求数据格式	 data	cookie操作	header操作	预期结果方式	 预期结果	result	数据

        function, is_run, condition, depend_key, url, method, gits, request_data, cookie, header, execpet_method, execpet, result, result_data = data

        print("执行：",function,is_run,condition,depend_key,url,method,gits,request_data,cookie,header,execpet_method,execpet,result,result_data)
        #print('执行：',casename,casenum,isrun,method,cookie)

if __name__ == "__main__":
    unittest.main()
