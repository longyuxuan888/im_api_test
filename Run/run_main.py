#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
base_path = os.path.abspath(os.path.join(os.getcwd(), ".."))  # 获取上级目录
sys.path.append(base_path)
from Base.base_request import request
from Util.codition_data import get_data
from Util.handle_cookie import write_cookie, get_cookie_value
from Util.handle_result import handle_result, handle_result_json, get_result_json
from Util.handle_header import get_header
import json
from Util.handle_excel import excel_data
from collections.abc import Iterable
from Util import Log
log = Log.MyLog()


class RunMain:

    def run_case(self):
        rows = excel_data.get_rows()
        for i in range(rows):
            cookie = None
            get_cookie = None
            header = None
            depend_data = None
            data = excel_data.get_rows_value(i + 2)
            is_run = data[2]
            if is_run == 'yes':
                is_depend = data[3]  # 获取前置条件
                if data[7] == "form":
                    data1 = json.loads(data[8])  # 读取用例中的数据字符串转成字典格式了
                if data[7] == "json":
                    data1 = json.loads(data[8])
                    data1 = json.dumps(data1)
                if is_depend:
                    depend_key = data[4]  # 获取依赖数据
                    depend_data = get_data(is_depend)
                    log.info("获取到的依赖数据：" + depend_data)
                    data1[depend_key] = depend_data
                    if data[10] == "yes":  # 通过Excel中的header值，控制是否需要把依赖数据的字典转出json格式
                        data1 = json.dumps(data1)  # 将依赖数据的字典转成json格式
                    if data[10] == "no":
                        pass
                method = data[6]
                url = data[5]
                is_header = data[10]
                excepect_method = data[11]
                excepect_result = data[12]
                cookie_method = data[9]
                if cookie_method == 'yes':
                    cookie = get_cookie_value('app')
                if cookie_method == 'write':
                    '''
                    必须是获取到cookie
                    '''
                    get_cookie = {"is_cookie": "app"}
                if is_header == 'yes':
                    header = get_header()

                res = request.run_main(method, url, data1, cookie, get_cookie, header)

                code = str(res['code'])
                message = res['msg']

                try:
                    if excepect_method == 'mec':
                        config_message = handle_result(url, code)
                        if message == config_message:
                            log.info('执行结果：' + str(res))
                            excel_data.excel_write_data(i + 2, 14, "通过")
                        else:
                            excel_data.excel_write_data(i + 2, 14, "失败")
                            excel_data.excel_write_data(i + 2, 15, json.dumps(res))
                            log.info('执行结果：' + str(res))

                    if excepect_method == 'code':
                        if excepect_result == code:
                            log.info('执行结果：' + str(res))
                            excel_data.excel_write_data(i + 2, 14, "通过")

                        else:
                            excel_data.excel_write_data(i + 2, 14, "失败")
                            excel_data.excel_write_data(i + 2, 15, json.dumps(res))
                            log.info('执行结果：' + str(res))

                    if excepect_method == 'json':
                        if code == 200:
                            status_str = 'susess'

                        else:
                            status_str = 'error'

                        excepect_result = get_result_json(url, status_str)
                        result = handle_result_json(res, excepect_result)
                        if result:
                            log.info('执行结果：' + str(res))
                            excel_data.excel_write_data(i + 2, 14, "通过")
                            excel_data.excel_write_data(i + 2, 15, json.dumps(res))
                        else:
                            excel_data.excel_write_data(i + 2, 14, "失败")
                            excel_data.excel_write_data(i + 2, 15, json.dumps(res))
                            log.info('执行结果：' + str(res))
                except ArithmeticError as e:
                    log.error(e)


if __name__ == "__main__":
    run = RunMain()
    run.run_case()
